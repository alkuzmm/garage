package com.example.web.controller;


import com.example.data.entity.Manufacturer;
import com.example.data.entity.Vehicle;
import com.example.service.ManufacturerService;
import com.example.service.VehicleService;
import com.example.web.dto.VehicleDto;
import com.example.web.validation.VehicleValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.PostUpdate;
import java.util.ArrayList;
import java.util.List;

@RestController
//@RequestMapping("/vehicles")
public class VehicleController {

    private final VehicleService vehicleService;
    private final ManufacturerService manufacturerService;

    @Autowired
    public VehicleController(VehicleService vehicleService, ManufacturerService manufacturerService) {
        this.vehicleService = vehicleService;
        this.manufacturerService = manufacturerService;
    }

    @GetMapping(value = "/vehicles", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<VehicleDto>> getAll() {
        List<VehicleDto> vehicles = new ArrayList<>();
        vehicleService.getAllVehicles().forEach(vehicle -> vehicles.add(VehicleDto.from(vehicle)));
        return new ResponseEntity<>(vehicles, HttpStatus.OK);
    }

    @PostMapping(value = "/vehicles/manufacturers/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity create(@PathVariable(name = "id") Long id, @RequestBody Vehicle vehicle) {
        vehicle.setManufacturer(manufacturerService.findById(id));
        VehicleValidator.validate(vehicle);
        vehicleService.save(vehicle);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/vehicles/{id}")
    public ResponseEntity delete(@PathVariable(name = "id") Long id) {
        vehicleService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping(value = "/vehicles/manufacturers")
    public ResponseEntity update(@RequestBody Vehicle vehicle) {
        VehicleValidator.validate(vehicle);
        vehicleService.save(vehicle);
        return new ResponseEntity(vehicle, HttpStatus.OK);
    }






}
