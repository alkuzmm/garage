package com.example.web.validation;

import com.example.data.entity.Manufacturer;
import com.example.data.entity.Vehicle;
import com.example.exception.ManufacturerValidationException;
import com.example.exception.VehicleValidationException;
import org.springframework.util.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class VehicleValidator {
    private static final String EMPTY_PROPERTY_EXCEPTION_MESSAGE = "Vehicle field parameter '%s' must be provided";
    private static final String REGEX_EXCEPTION_MESSAGE = "Vehicle field parameter '%s' must match these parameters: '%s'";

    private static String VALID_VIN_REGEX = "^.+$";

    public static void validate(Vehicle vehicle) throws VehicleValidationException {
        validateNotEmptyProperty(vehicle.getEngineCapacity(), "engineCapacity");
        validateNotEmptyProperty(vehicle.getSeats(), "seats");
        validateNotEmptyProperty(vehicle.getWheels(), "wheels");
//        validateWithRegularExpression(vehicle.getVinNumber(), VALID_VIN_REGEX, "vinNumber",
//                "Incorrect VIN number");
        validateNotEmptyProperty(vehicle.getVinNumber(), "vinNumber");
        validateNotEmptyProperty(vehicle.getVehicleMass(), "vehicleMass");
        validateNotEmptyProperty(vehicle.getColor(), "color");
        validateNotEmptyProperty(vehicle.getModel(), "model");
        validateNotEmptyProperty(vehicle.getManufacturer(), "manufacturer");
    }

    private static void validateNotEmptyProperty(Object value, String propertyName) throws VehicleValidationException {
        if (value == null || StringUtils.isEmpty(value)) {
            throw new VehicleValidationException(String.format(EMPTY_PROPERTY_EXCEPTION_MESSAGE, propertyName));
        }
    }

    private static void validateWithRegularExpression(Object value, String regex, String propertyName, String exceptionMessage) throws VehicleValidationException {
        Matcher matcher = Pattern.compile(regex).matcher(String.valueOf(value));
        if (!matcher.matches()) {
            throw new VehicleValidationException(String.format(REGEX_EXCEPTION_MESSAGE, propertyName, exceptionMessage));
        }
    }
}