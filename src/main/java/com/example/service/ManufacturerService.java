package com.example.service;

import com.example.data.entity.Manufacturer;

import java.util.List;

public interface ManufacturerService {

    Manufacturer findById(Long id);

    Manufacturer save(Manufacturer manufacturer);

    List<Manufacturer> findAll();
}
