package com.example.service.implemented;

import com.example.data.dao.ManufacturerDAO;
import com.example.service.ManufacturerService;
import com.example.data.entity.Manufacturer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ManufacturerServiceImpl implements ManufacturerService {

    private final ManufacturerDAO manufacturerDAO;

    @Autowired
    public ManufacturerServiceImpl(ManufacturerDAO manufacturerDAO) {
        this.manufacturerDAO = manufacturerDAO;
    }

    @Override
    public Manufacturer findById(Long id) {
        return manufacturerDAO.findById(id);
    }

    @Override
    public Manufacturer save(Manufacturer manufacturer) {
        return manufacturerDAO.save(manufacturer);
    }

    @Override
    public List<Manufacturer> findAll() {
        return manufacturerDAO.findAll();
    }
}
