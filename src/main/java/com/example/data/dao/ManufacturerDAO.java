package com.example.data.dao;

import com.example.data.entity.Manufacturer;

import java.util.List;

public interface ManufacturerDAO {

    Manufacturer findById(Long id); //R - read

    Manufacturer save(Manufacturer manufacturer);

    List<Manufacturer> findAll();
}
