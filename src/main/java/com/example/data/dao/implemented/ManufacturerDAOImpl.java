package com.example.data.dao.implemented;

import com.example.data.dao.ManufacturerDAO;
import com.example.data.entity.Manufacturer;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class ManufacturerDAOImpl implements ManufacturerDAO {

    @PersistenceContext
    private EntityManager em;

    @Override
    public Manufacturer findById(Long id) {
        return (Manufacturer)
                em.createQuery("select m from Manufacturer m where m.id = :id")
                .setParameter("id", id)
                .getSingleResult();

    }

    @Override
    public Manufacturer save(Manufacturer manufacturer) {
        em.persist(manufacturer);
        return manufacturer;
    }

    @Override
    public List<Manufacturer> findAll() {
        return em.createQuery("select m from Manufacturer m").getResultList();
    }
}
