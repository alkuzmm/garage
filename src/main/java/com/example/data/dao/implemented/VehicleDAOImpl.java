package com.example.data.dao.implemented;

import com.example.data.dao.VehicleDAO;
import com.example.data.entity.Vehicle;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Transactional
@Repository("vehicleDAO")
@Slf4j
public class VehicleDAOImpl implements VehicleDAO {

    @PersistenceContext
    private EntityManager em;

    @Override
    public Vehicle findById(Long id) {
        return null;
    }

    @Override
    public Vehicle save(Vehicle vehicle) {
        em.persist(vehicle);
        return vehicle;
    }

    @Override
    public List<Vehicle> findAll() {
        return em.createQuery("select v from Vehicle v").getResultList();
    }

    @Override
    public void delete(Long vehicleId) {
        em.createQuery("delete from Vehicle v where v.id = :vehicleId")
                .setParameter("vehicleId", vehicleId)
                .executeUpdate();
    }

//    @Override
//    public void update(Vehicle vehicle) {
//        em.createQuery("update Vehicle v set v.engineCapacity = :engineCapacity," +
//                "v.seats = :seats, v.wheels = :wheels, v.vinNumber = :vinNumber, v.vehicleMass = :vehicleMass," +
//                "v.color = :color, v.model = :model, v.manufacturer = :manufacturer where v.id = : id")
//        .setParameter("engineCapacity" = String.valueOf(Double.valueOf((vehicle.getEngineCapacity()))
//
//        ;

//    }

    @Override
    public void update(Vehicle vehicle) {

    }
}
