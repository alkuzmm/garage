package com.example.data.web.controller;

import com.example.service.ManufacturerService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;

import java.awt.*;
import java.util.List;


import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;


import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class ManufacturerControllerMockTest extends ControllerBaseTest {

    @MockBean
    private ManufacturerService manufacturerService;

    @Test
    void whenEmptyManufacturersListShouldRespondOkTest() throws Exception{
        when(manufacturerService.findAll()).thenReturn(List.of());

        mockMvc.perform(get("/manufacturers")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(0)))
                .andExpect(status().isOk());
    }
}
